public class Bunny {

    private String name;
    private int age;
    private boolean lovesEaster;

    public Bunny(String name, int age, boolean lovesEaster) {
        this.name = name;
        this.age = age;
        this.lovesEaster = lovesEaster;
    }

    @Override
    public String toString() {
        return "Bunny{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", lovesEaster=" + lovesEaster +
                '}';
    }
}
